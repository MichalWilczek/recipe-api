variable "prefix" {
  default = "recipe-api"
}

variable "project" {
  default = "recipe-api"
}

variable "contact" {
  default = "mich.wilcz@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-api-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "923833213406.dkr.ecr.us-east-1.amazonaws.com/recipe-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "923833213406.dkr.ecr.us-east-1.amazonaws.com/recipe-api-proxy"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "michal-wilczek-recipe-api.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}